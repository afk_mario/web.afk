const path = require(`path`);
const { makeLogPath, makeProjectPath, makeWikiPath } = require(`./src/utils`);

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;
  const wikiSingleTemplate = require.resolve(
    `./src/components/wiki-single/index.js`
  );
  const projectSingleTemplate = require.resolve(
    `./src/components/project-single/index.js`
  );

  const logSimpleTemplate = require.resolve(
    `./src/components/log-single/index.js`
  );

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              slug
            }
          }
        }
      }
      ellugar {
        allPosts {
          edges {
            node {
              id
              slug
            }
          }
        }
        allProjects {
          edges {
            node {
              id
              slug
            }
          }
        }
      }
    }
  `);

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    console.Error(result.errors);
    return;
  }

  const {
    ellugar: { allPosts, allProjects },
    allMarkdownRemark,
  } = result.data;

  allPosts.edges.forEach(({ node, cursor }) => {
    createPage({
      path: makeLogPath(node),
      component: logSimpleTemplate,
      context: {
        id: node.id,
        cursor,
      },
    });
  });

  allProjects.edges.forEach(({ node, cursor }) => {
    createPage({
      path: makeProjectPath(node),
      component: projectSingleTemplate,
      context: {
        id: node.id,
        cursor,
      },
    });
  });

  allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: `/wiki/${node.frontmatter.slug}`,
      component: wikiSingleTemplate,
      context: {
        slug: node.frontmatter.slug,
      },
    });
  });
};
