const postcssPresetEnv = require(`postcss-preset-env`);
// const postcssImport = require(`postcss-import`);

module.exports = () => ({
  plugins: [
    // postcssImport({}),
    postcssPresetEnv({
      stage: 0,
      features: {
        "custom-properties": {
          warnings: false,
        },
        "custom-media-queries": {
          importFrom: "src/styles/queries.css",
        },
      },
    }),
  ],
});
