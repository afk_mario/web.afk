---
slug: "game-dev-3d"
date: "2021-10-05"
title: "Game dev 3D"
publish: true
---

Learning how to do 3D games.

## Import pipeline

It seems there is no consensus on how to export a 3D file to be used in a game engine, inclined to think that GLTF 2.0 is the best option.

All material/shader languages are slightly different so materials are the hardest part to bring from a 3D software in to a game engine.

One would think that as Blender and Godot are both open source they would use a standard for basic materials but it doesn't seem to be the case. In general I think it's easier to learn how to do materials in Blender or Unity and then figuring out how to port them to Godot.

- [FBX importer rewritten for Godot 3.2.4 and later](https://godotengine.org/article/fbx-importer-rewritten-for-godot-3-2-4)
- [Why we should all support glTF 2.0 as THE standard asset exchange format for game engines ](https://godotengine.org/article/we-should-all-use-gltf-20-export-3d-assets-game-engines)

## Shaders / Materials

### Ambient occlussion

- [The Pros and Cons of Ambient Occlusion and How to Use It Effectively](https://www.gamedesigning.org/learn/ambient-occlusion/)

### General resources

- [The book of shaders](https://thebookofshaders.com/)
- [Smoothstep: The most useful function](https://www.youtube.com/watch?v=60VoL-F-jIQ)

### Toon

*Blender*
- [Tutorial: Procedural hatching and manga shaders for EEVEE Blender](https://www.youtube.com/watch?v=2ZR5XIjBmho)
- [Toon shader](https://www.youtube.com/watch?v=TpWI2rU8iF0)
- [Outline and toon shading (esp)](https://www.youtube.com/watch?v=hyhUW1Jjsns)

### Triplanar shaders for terrain

*Unity*

- [Minions Art triplanar shader](https://www.patreon.com/posts/16714688)
- [Normal Mapping for a Triplanar Shader](https://bgolus.medium.com/normal-mapping-for-a-triplanar-shader-10bf39dca05a)
- [Triplanar Mapping](http://www.martinpalko.com/triplanar-mapping/)

*Godot*

- [Jotson Minions Art port to Godot](https://github.com/jotson/godot3-triplanar-terrain-demo)
- [Arcande Hive Games Videos](https://www.youtube.com/watch?v=ZR8Qyv5IlQs)
