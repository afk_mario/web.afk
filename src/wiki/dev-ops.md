---
slug: "dev-ops"
date: "2021-02-03"
title: "Dev ops"
publish: true
---

# Django & Docker & Traefik


## Notes on errors

I migrated a django website with i18n enabled from using nginx to traefik and was getting the following error

```
  "msg": "vulcand/oxy/buffer: failed to read response, err: no data ready",
```

Reading [this issue](https://github.com/traefik/traefik/issues/4456) it turns out that when django redirected to the laguage version of the page the user was triying to access it didn't setup the *Content-Length* header in the response, this was because the locale middleware was defined before the common middleware.
