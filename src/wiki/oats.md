---
slug: "oats"
date: "2021-12-01"
title: "Oats"
publish: true
---

# Avena de arandanos con platano (15-20 mins)

## Ingredientes

- 1 1/2 taza de avena
- 3 1/2 taza de agua
- 1 1/2 taza de leche vegetal

- 1 1/2 cucharada de chia
- 1 cucharada de arandanos (+- dependiendo de que tan dulce te gusta)
- 1 cucharada de crema de cacahuate
- 1 tapa de vainilla
- 1 1/2 vara de canela
- 1 platano en rodajas
- 1 cucharada de semillas de girasol

## Instrucciones

1. Doras un poco la avena en la cazuela
2. Agregas el agua y la leche vegetal
3. Agregas el platano
4. Agregas los arandanos, chia, canela y vainilla
5. Agregas la crema de cacahuate
6. Mezclas todo aplastando ligeramente el platano para que se disuelva
7. En cuanto empiece a hervir puedes dejarlo un poco más de tiempo dependiendo de tu gusto de que tan liquida prefieres
8. La sirves inmediatamente en un un recipiente
9. La dejas enfriar 3 minutos
10. Agregas las semillas de girasol

# Avena de dátil, cardamomo y plátano (8 hrs)

## Ingredientes

- 1 1/2 taza de avena
- 1 taza de leche vegetal

- 1 cucharadita de cardamomo fresco
- 1 1/2 cucharada de chía
- 1 dátil medjool
- 1 tapa de vainilla
- 1 plátano en rodajas

## Instrucciones

- Meter en la licuadora o nutribullet hasta que se disuelvan:
    + chía
    + dátil
    + vainilla
    + plátano
    + leche
    + cardamomo
- En un mason jar meter la avena
- Agregar la mezcla de la licuadora
- agitas el mason para que se mezcle bien todo
- Refrigerar 8hrs
